import { Animation, AudioListener, DirectionalLight, FPSController, Object3D, Scene, serialization, ShadowType, Vector3, View } from 'feng3d';
import { load } from './src';

const scene = serialization.setValue(new Object3D(), { name: 'Untitled' }).addComponent(Scene);
scene.background.setTo(0.2784, 0.2784, 0.2784);
scene.ambientColor.setTo(0.4, 0.4, 0.4);

const camera = Object3D.createPrimitive('Camera', { name: 'Main Camera' });
camera.addComponent(AudioListener);
camera.addComponent(FPSController);
camera.position = new Vector3(100, 200, 300);
camera.lookAt(new Vector3(), Vector3.up);
scene.object3D.addChild(camera);

const directionalLight = serialization.setValue(new Object3D(), { name: 'DirectionalLight' });
directionalLight.addComponent(DirectionalLight).shadowType = ShadowType.Hard_Shadows;
directionalLight.rx = 50;
directionalLight.ry = -30;
directionalLight.y = 3;
scene.object3D.addChild(directionalLight);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const engine = new View(undefined, scene);

// const cube = Object3D.createPrimitive('Cube');
// scene.object3D.addChild(cube);

load('models/fbx/Samba Dancing.fbx').then((object3D) =>
{
    scene.object3D.addChild(object3D);

    const animation = object3D.getComponent(Animation);
    animation.isplaying = true;

    console.log(object3D);
});
