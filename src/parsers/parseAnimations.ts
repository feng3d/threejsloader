import { Animation, AnimationClip, Object3D, PropertyClip, PropertyClipPathItemType, SkeletonComponent } from 'feng3d';
import * as THREE from 'three';

export function parseAnimations(object3d: THREE.Object3D, object3D: Object3D)
{
    if (object3d.animations && object3d.animations.length > 0)
    {
        const animation = object3D.addComponent(Animation);
        for (let i = 0; i < object3d.animations.length; i++)
        {
            const animationClip = parseAnimationClip(object3d.animations[i]);
            animation.animations.push(animationClip);
            animation.animation = animation.animations[0];
        }

        // 添加骨骼组件
        object3D.addComponent(SkeletonComponent);
    }
}

function parseAnimationClip(animationClipData: THREE.AnimationClip)
{
    //
    const animationClip = new AnimationClip();

    animationClip.name = animationClipData.name;
    animationClip.length = animationClipData.duration * 1000;
    animationClip.propertyClips = [];

    const tracks = animationClipData.tracks;
    const len = tracks.length;
    for (let i = 0; i < len; i++)
    {
        const propertyClip = parsePropertyClip(tracks[i]);
        animationClip.propertyClips.push(propertyClip);
    }

    return animationClip;
}

function parsePropertyClip(keyframeTrack: THREE.KeyframeTrack)
{
    const propertyClip = new PropertyClip();

    const trackName: string = keyframeTrack.name;
    const result = (/(\w+)\.(\w+)/).exec(trackName) as RegExpExecArray;
    propertyClip.path = <any>[
        [PropertyClipPathItemType.Object3D, result[1]],
    ];

    switch (result[2])
    {
        case 'position':
            propertyClip.propertyName = 'position';
            break;
        case 'scale':
            propertyClip.propertyName = 'scale';
            break;
        case 'quaternion':
            propertyClip.propertyName = 'orientation';
            break;
        default:
            console.warn(`没有处理 propertyName ${result[2]}`);
            break;
    }

    propertyClip.times = [];
    propertyClip.values = [];
    const times = keyframeTrack.times;
    const values = keyframeTrack.values;

    const len = times.length;
    switch (keyframeTrack.ValueTypeName)
    {
        case 'vector':
            propertyClip.type = 'Vector3';
            for (let i = 0; i < len; i++)
            {
                propertyClip.times.push(times[i] * 1000);
                propertyClip.values.push(values[i * 3], values[i * 3 + 1], values[i * 3 + 2]);
            }
            break;
        case 'quaternion':
            propertyClip.type = 'Quaternion';
            for (let i = 0; i < len; i++)
            {
                propertyClip.times.push(times[i] * 1000);
                propertyClip.values.push(values[i * 4], values[i * 4 + 1], values[i * 4 + 2], values[i * 4 + 3]);
            }
            break;
        default:
            console.warn(`没有提供解析 ${keyframeTrack.ValueTypeName} 类型Track数据`);
            break;
    }

    return propertyClip;
}
