import { Material, Object3D, Renderable } from 'feng3d';
import { Mesh } from 'three';
import { parseGeometry } from './parseGeometry';

export function parseMesh(mesh: Mesh, object3D: Object3D)
{
    const model = object3D.addComponent(Renderable);
    model.geometry = parseGeometry(mesh.geometry);
    model.material = new Material();
    model.material.renderParams.cullFace = 'NONE';
}
