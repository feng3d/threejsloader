import { Object3D, serialization } from 'feng3d';
import * as THREE from 'three';
import { Mesh, PerspectiveCamera, SkinnedMesh } from 'three';
import { parseAnimations } from './parseAnimations';
import { parseMesh } from './parseMesh';
import { parsePerspectiveCamera } from './parsePerspectiveCamera';
import { parseSkinnedMesh } from './parseSkinnedMesh';
import { parseTransform } from './parseTransform';

export function parseObject3D(object3d: THREE.Object3D, parent?: Object3D): Object3D
{
    const object3D = serialization.setValue(new Object3D(), { name: object3d.name });
    parseTransform(object3d, object3D);

    if (parent)
    {
        parent.addChild(object3D);
    }

    switch (object3d.type)
    {
        case 'PerspectiveCamera':
            parsePerspectiveCamera(object3d as PerspectiveCamera, object3D);
            break;
        case 'SkinnedMesh':
            parseSkinnedMesh(object3d as SkinnedMesh, object3D);
            break;
        case 'Mesh':
            parseMesh(object3d as any as Mesh, object3D);
            break;
        case 'Group':
            break;
        case 'Bone':
            // Bone 由SkeletonComponent自动生成，不用解析
            break;
        default:
            console.warn(`没有提供 ${object3d.type} 类型对象的解析`);
            break;
    }

    parseAnimations(object3d, object3D);

    object3d.children.forEach((element) =>
    {
        parseObject3D(element, object3D);
    });

    return object3D;
}
