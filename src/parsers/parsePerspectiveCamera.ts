import { Camera, Object3D, PerspectiveLens } from 'feng3d';
import { PerspectiveCamera } from 'three';

export function parsePerspectiveCamera(perspectiveCamera: PerspectiveCamera, object3D: Object3D)
{
    const perspectiveLen = new PerspectiveLens();

    perspectiveLen.near = perspectiveCamera.near;
    perspectiveLen.far = perspectiveCamera.far;
    perspectiveLen.aspect = perspectiveCamera.aspect;
    perspectiveLen.fov = perspectiveCamera.fov;

    object3D.addComponent(Camera).lens = perspectiveLen;
}
