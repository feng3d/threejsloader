import { Material, Object3D, SkeletonComponent, SkinnedMeshRenderer } from 'feng3d';
import { SkinnedMesh } from 'three';
import { parseGeometry } from './parseGeometry';
import { parseSkinnedSkeleton } from './parseSkinnedSkeleton';

export function parseSkinnedMesh(skinnedMesh: SkinnedMesh, object3D: Object3D)
{
    const skinnedModel = object3D.addComponent(SkinnedMeshRenderer);
    skinnedModel.geometry = parseGeometry(skinnedMesh.geometry);
    skinnedModel.material = new Material();
    skinnedModel.material.renderParams.cullFace = 'NONE';
    console.assert(skinnedMesh.bindMode === 'attached');
    const skeletonComponent = object3D.getComponentInParent(SkeletonComponent);
    parseSkinnedSkeleton(skeletonComponent, skinnedMesh.skeleton);
}
