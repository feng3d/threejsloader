import { Object3D, Quaternion, Vector3 } from 'feng3d';
import * as THREE from 'three';

export function parseTransform(object3d: THREE.Object3D, object3D: Object3D)
{
    object3D.position = new Vector3(object3d.position.x, object3d.position.y, object3d.position.z);
    object3D.orientation = new Quaternion(object3d.quaternion.x, object3d.quaternion.y, object3d.quaternion.z, object3d.quaternion.w);
    object3D.scale = new Vector3(object3d.scale.x, object3d.scale.y, object3d.scale.z);
}
