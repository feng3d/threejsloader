import { globalEmitter, pathUtils } from 'feng3d';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { parseObject3D } from './parsers/parseObject3D';

export async function load(url: string)
{
    const loader = new FBXLoader();
    const object = await loader.loadAsync(url);

    const object3D = parseObject3D(object);

    object3D.name = pathUtils.nameWithOutExt(url);
    globalEmitter.emit('asset.parsed', object3D);

    return object3D;
}
